/*
Navicat MySQL Data Transfer

Source Server         : vagrant
Source Server Version : 50727
Source Host           : localhost:3306
Source Database       : lesson

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2019-11-03 10:07:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES ('1', '1', '1', '2', '2019-10-30 16:15:00', '2019-10-30 16:15:00');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('2', 'Books', '0', '2019-10-27 10:05:41', '2019-10-27 10:05:41');
INSERT INTO `categories` VALUES ('3', 'Pictures', '0', '2019-10-30 16:13:43', '2019-10-30 16:13:43');
INSERT INTO `categories` VALUES ('4', 'TV', '11', '2019-11-02 08:38:40', '2019-11-02 08:38:40');
INSERT INTO `categories` VALUES ('5', 'Smartphones', '11', '2019-11-02 08:38:55', '2019-11-02 08:38:55');
INSERT INTO `categories` VALUES ('6', 'Laptops', '11', '2019-11-02 08:39:11', '2019-11-02 08:39:11');
INSERT INTO `categories` VALUES ('7', 'Programming', '2', '2019-11-02 08:39:26', '2019-11-02 08:39:26');
INSERT INTO `categories` VALUES ('8', 'Fantasy', '2', '2019-11-02 08:39:32', '2019-11-02 08:39:32');
INSERT INTO `categories` VALUES ('9', 'Apple', '5', '2019-11-02 08:39:57', '2019-11-02 08:39:57');
INSERT INTO `categories` VALUES ('10', 'Samsung', '4', '2019-11-02 08:40:07', '2019-11-02 08:40:07');
INSERT INTO `categories` VALUES ('11', 'Electronics', '0', '2019-10-27 10:05:06', '2019-10-27 10:05:06');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `price` decimal(10,2) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('5', 'iphone x', '18000.00', '8', '9', '2019-10-27 10:26:43', '2019-10-27 10:26:43');
INSERT INTO `products` VALUES ('8', 'SAMSUNG U50', '12000.00', '3', '10', '2019-10-27 10:35:48', '2019-10-27 10:35:48');
INSERT INTO `products` VALUES ('9', 'iphone xr', '16000.00', '2', '9', '2019-10-27 10:35:48', '2019-10-27 10:35:48');
INSERT INTO `products` VALUES ('10', 'Lenovo Thinkpad T', '40000.00', '8', '6', '2019-10-27 10:35:48', '2019-10-27 10:35:48');
INSERT INTO `products` VALUES ('13', 'Mona Lisa', '500.00', '8', '3', '2019-10-27 10:26:43', '2019-10-27 10:26:43');
INSERT INTO `products` VALUES ('14', 'Head First PHP', '300.00', '10', '7', '2019-11-02 08:43:02', '2019-11-02 08:43:02');
INSERT INTO `products` VALUES ('15', 'Harry Potter', '200.00', '100', '8', '2019-11-02 08:43:31', '2019-11-02 08:43:31');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'dev', 'test@test.com', '9d098171d573190cab695a8d56f3602f720b62c4', '2019-10-30 16:15:18', '2019-10-30 16:15:18');
SET FOREIGN_KEY_CHECKS=1;
