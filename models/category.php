<?php
    
    function getCategories($db)
    {
        $stmt = $db->query("SELECT * FROM categories");
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function fetchCategoryTreeList($db, $parent = 0, $userTreeArray = '') {
        if (!is_array($userTreeArray)){
            $userTreeArray = [];
        }
        $sql = "SELECT * FROM `categories` WHERE 1 AND `parent_id` = $parent ORDER BY id ASC";
        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC); 
        if (count($result) > 0)
        {
            $userTreeArray[] = "<ul>";
            foreach($result as $row){
                $userTreeArray[] = "<li><a 
                    href='/index.php?category=".$row['id']."'>". $row["title"]."</a></li>";
                $userTreeArray = fetchCategoryTreeList($db, $row["id"], $userTreeArray);
            }
            $userTreeArray[] = "</ul><br/>";
        }
        return $userTreeArray;
    }

    function getCategoriesTree($list, $pk = 'id', $pid = 'parent_id', $child = '_child', $root = 0)
    {
        $tree = [];
        if (is_array($list))
        {
            $refer = [];
            foreach ($list as $key => $data)
            {
                $refer[$data[$pk]] =& $list[$key];
            }
            foreach ($list as $key => $data)
            {
                $parentId = $data[$pid];
                if ($root == $parentId)
                {
                    $tree[] =& $list[$key];
                }
                else
                {
                    if (isset($refer[$parentId]))
                    {
                        $parent =& $refer[$parentId];
                        $parent[$child][] =& $list[$key];
                    }
                }
            }
        }
        return $tree;
    }
?>