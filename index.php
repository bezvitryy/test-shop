<?php
    require_once("config.php");
    require_once(ROOT_PATH."/models/product.php");
    require_once(ROOT_PATH."/models/category.php");

    $categoryId = $_GET['category'] ?? 0;
    $categoryId = (int) $categoryId;
    $products = getHomeProducts($pdo,$categoryId);
    $categories = getCategories($pdo);
    $tree = fetchCategoryTreeList($pdo);
    
    require_once(ROOT_PATH."/templates/home.php");
?>